package pg_plugin_interface

import (
	"fmt"
	jsoniter "github.com/json-iterator/go"
)

func HandleDispatch(params []string) string {
	dispatcher, err := GetHandler(params[0])
	if err != nil {
		return parseDispatchResult(params, DispatchResult{
			Name:  params[0],
			Data:  nil,
			Error: err.Error(),
		})
	}
	if len(params) > 1 {
		err = jsoniter.Unmarshal([]byte(params[1]), &dispatcher)
	}
	if err != nil {
		return getErrorDispatchResult(params[0], err.Error())
	}
	return parseDispatchResult(params, dispatcher.Execute())
}

func parseDispatchResult(params []string, res DispatchResult) string {
	str, err := jsoniter.Marshal(res)
	if err != nil {
		return getErrorDispatchResult(params[0], err.Error())
	}
	return string(str)
}

func getErrorDispatchResult(name, err string) string {
	return fmt.Sprintf("{\"name\":\"%v\",\"data\":null,\"error\":\"%v\"}", name, err)
}
