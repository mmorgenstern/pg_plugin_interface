package pg_plugin_interface

import (
	"errors"
	"fmt"
)

type DispatcherGetter = func() DispatchHandler

var handlers = make(map[string]DispatcherGetter)

func GetHandler(name string) (DispatchHandler, error) {
	if handlers[name] == nil {
		return nil, errors.New(fmt.Sprintf("no handler found with name %v", name))
	}
	return handlers[name](), nil
}

func Register(event DispatcherGetter) {
	d := event()
	if handlers[d.GetName()] != nil && !d.IsOverwritable() {
		return
	}
	handlers[d.GetName()] = event
}

func Unregister(name string) {
	delete(handlers, name)
}
