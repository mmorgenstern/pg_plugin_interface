package pg_plugin_interface

import (
	"fmt"
	"testing"
)

type TestDispatcher struct {
	Value string `json:"value"`
}

func (td *TestDispatcher) GetName() string {
	return "test"
}

func (td *TestDispatcher) IsOverwritable() bool {
	return true
}

func (td *TestDispatcher) Execute() DispatchResult {
	return DispatchResult{
		Name:  td.GetName(),
		Data:  td.Value,
		Error: "",
	}
}

func TestRegister(t *testing.T) {
	name := "test"
	Register(func() DispatchHandler {
		return &TestDispatcher{}
	})
	h, err := GetHandler(name)
	if err != nil {
		t.Error(err)
	}
	if h == nil {
		t.Errorf("handler %v is nil", name)
	}
	Unregister(name)
	_, err = GetHandler(name)
	if err == nil {
		t.Error("expect a error but get nil")
	}
	expectedErrorMessage := fmt.Sprintf("no handler found with name %v", name)
	if err != nil && err.Error() != expectedErrorMessage {
		t.Errorf("expect error message %v to be %v", err.Error(), expectedErrorMessage)
	}
}

func TestHandleDispatch(t *testing.T) {
	name := "test"
	Register(func() DispatchHandler {
		return &TestDispatcher{}
	})

	res := HandleDispatch([]string{
		name, "{\"value\":\"Hello\"}",
	})
	expectRes := "{\"name\":\"test\",\"data\":\"Hello\",\"error\":\"\"}"
	if res != expectRes {
		t.Errorf("expect %v to be %v", res, expectRes)
	}

	Unregister(name)
}

func TestHandleDispatchWithoutParameter(t *testing.T) {
	name := "test"
	Register(func() DispatchHandler {
		return &TestDispatcher{}
	})

	res := HandleDispatch([]string{
		name,
	})
	expectRes := "{\"name\":\"test\",\"data\":\"\",\"error\":\"\"}"
	if res != expectRes {
		t.Errorf("expect %v to be %v", res, expectRes)
	}

	Unregister(name)
}
