package pg_plugin_interface

type DispatchResult struct {
	Name  string      `json:"name"`
	Data  interface{} `json:"data"`
	Error string      `json:"error"`
}

type DispatchHandler interface {
	GetName() string
	IsOverwritable() bool
	Execute() DispatchResult
}
